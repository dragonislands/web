'use strict';

var app = angular.module('dragon.faq', ['ngRoute', 'ngSanitize']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/faq', {
    templateUrl: '/faq/faq.html',
    controller: 'FaqCtrl'
  });
}])

.controller('FaqCtrl', function($scope, $firebaseArray) {
  var ref = firebase.database().ref().child("faq");
  // create a synchronized array
  $scope.faqs = $firebaseArray(ref);
});
