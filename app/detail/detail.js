'use strict';

var app = angular.module('dragon.detail', ['ngRoute']);

app.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/edit/:id', {
      templateUrl: '/detail/detail.html',
      controller: 'EditCtrl',
      resolve: {
        "currentAuth": [
          "Auth", function(Auth) {
            return Auth.$requireSignIn();
          }
        ]
      }
    });
  }
])
  
  .controller('EditCtrl', function($scope, $firebaseObject, $routeParams, $location, Auth) {
    var id = $routeParams.id;
    var ref = firebase.database().ref().child("clans/" + id);
    $scope.user = Auth.$getAuth();
    // create a synchronized object
    var clan = $firebaseObject(ref);
    clan.$loaded(function() {
        if($scope.user.uid != clan.uid) {
          $location.path('/clans');
        }
        else {
          $scope.clan = clan;
        }
      });
    $scope.delClan = function() {
      $scope.clan.$remove();
      $location.path('/clans');
    }
  });
