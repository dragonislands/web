'use strict';

var app = angular.module('dragon.add.faq', ['ngRoute']);

app.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/add/faq', {
      templateUrl: '/add/faq/add.faq.html',
      controller: 'AddFAQCtrl',
      resolve: {
        "currentAuth": ["Auth", function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    });
  }
])
  
  .controller('AddFAQCtrl', function($scope, $firebaseArray, Auth) {
    $scope.user = Auth.$getAuth();

    var ref = firebase.database().ref().child("faq");
    // create a synchronized array
    $scope.faqs = $firebaseArray(ref);
    // add new items to the array
    // the message is automatically added to our Firebase database!
    $scope.addFAQ = function() {
      $scope.faqs.$add({
        q: $scope.newQuestion,
        a: $scope.newAnswer
      })
        .then(function() {
          $scope.success = true;
          $scope.q = $scope.newQuestion;
          $scope.a = $scope.newAnswer;
          $scope.newQuestion = "";
          $scope.newAnswer = "";
      });
    };
  });