'use strict';

var app = angular.module('dragon.add', ['ngRoute']);

app.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/add', {
      templateUrl: '/add/add.html',
      controller: 'AddCtrl',
      resolve: {
        "currentAuth": [
          "Auth", function(Auth) {
            return Auth.$requireSignIn();
          }
        ]
      }
    });
  }
])
  
  .controller('AddCtrl', function($scope, $firebaseArray, $location, Auth) {
    $scope.user = Auth.$getAuth();
    
    var ref = firebase.database().ref().child("clans");
    // create a synchronized array
    $scope.clans = $firebaseArray(ref);
    // add new items to the array
    // the message is automatically added to our Firebase database!
    $scope.addClan = function() {
      $scope.clans.$add({
        name: $scope.newClanName,
        desc: $scope.newClanDesc,
        id: $scope.newClanID,
        uid: $scope.user.uid
      });
      $location.path('/clans');
    };
  });
